# Analysis of Dynamic Features in Time Series Gene Expression Data

This repository contains the data and code used for paper titled "Conservation of dynamic characteristics of transcriptional regulatory elements in periodic biological processes".

### 1) Prerequisites

pyDL, one of the periodicity algorithms used in this study, makes use of MPI for parallelization.  Prior to installation be sure to have an updated Python and MPI installation:

* Interpreter
    ```
    Python >=3.6.0 via Anaconda
    ```

* MPI
    If you do not have `MPI` installed:
    ```
    For MacOS
    $ brew install openmpi
    $ mpiexec --version
    
    For Ubuntu
    $ sudo apt-get install openmpi-bin
    $ mpiexec --version
    ```

* cmake  
    If you do not have `cmake` installed:
    ```
    For MacOS
    $ brew install cmake
    $ cmake --version
    
    For Ubuntu
    $ sudo apt-get -y install cmake
    $ cmake --version
    ```
  
### 2) Python Dependencies

* pandas
* numpy
* configobj
* joblib
* scikit-learn

### 3) Jupyter Notebooks for Reproducing Analyses and Figures

To reproduce the paper's analyses and figures, run these notebooks in this order. Please read the notebooks carefully before use. 

* 1_data_processing.ipynb
* 2_run_periodicity_analysis.ipynb
* 3_convert_mouse_probes_2_ensembl.ipynb
* 4_duplicate_probe_filtering.ipynb
* 5_precision-recall_analysis.ipynb
* 6_increasing_permutation_analysis.ipynb
* 7_unique_scores_comparison.ipynb
* 8_manuscript_figures.ipynb


### Repo Contents
```
dynamic_features_analysis
├── README.md
├── Reproduce_Results
│   ├── ... (Same directory structure as "Results" directory)
├── Results
│   ├── Arth_Permutations_test
│   │   ├── ... (Results from Notebooks 6 and 7)
│   ├── P-R_results
│   │   ├── ... (Results from Notebook 5)
│   ├── cell_cycle
│   │   └── Scerevisiae
│   │       ├── microarray
│   │       │   └── ... (Results from Notebook 2 and 4)
│   │       └── rnaseq
│   │           └── ... (Results from Notebook 2)
│   └── circadian_clock
│       ├── athaliana
│       │   ├── LDHC
│       │   │   └── ... (Results from Notebook 2 and 4)
│       │   └── LL_LDHC
│       │       └── ... (Results from Notebook 2 and 4)
│       └── mouse_liver
│           ├── microarray
│           │   ├── ... (Results from Notebook 2 and 3)
│           └── rnaseq
│               └── ... (Results from Notebook 2)
├── annotation_files
│   ├── TF_list
│   │   ├── ... (Files containing lists of TFs for each species)
│   ├── annot_files
│   │   └── ... (Files used for converting probe ids)
│   └── core_node_list
│       ├── ... (Files containing lists of core genes for each species)
├── config_files
│   ├── ... (Configuration files for running each dataset through the periodicity algorithms)
├── scripts
│   ├── ... (Jupter Notebooks for reproducing the analyses and figures in this study)
│   ├── pydl
│   │   ├── ... (De Lichtenberg code - this is a git submodule)
│   ├── pyjtk
│   │   ├── ... (JTK-CYCLE code - this is a git submodule)
│   └── python_scripts
│       ├── ... (Python scripts used within the Jupyter Notebooks)
└── time_series_data
    ├── cell_cycle
    │   ├── ... (Processed data used in the analyses)
    │   └── original_data
    │       ├── ... (Original data)
    └── circadian_clock
        ├── ... (Processed data used in the analyses)
        └── original_data
            ├── ... (Original data)
```