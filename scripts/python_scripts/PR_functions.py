import os
import pandas as pd
from inspect import signature
from matplotlib import pyplot as plt, rcParams
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
rcParams.update({"svg.fonttype": "none"})

TF_dir = "../annotation_files/TF_list"
core_node_dir = "../annotation_files/core_node_list"
pydl_file = "pydl_results.tsv"
dlxjtk_file = "dlxjtk_results.tsv"


def P_and_N_labels(p_list, pn_df):
    p_labels = len(p_list)
    n_labels = len([gene for gene in pn_df.index.tolist() if gene not in p_list])
    total_labels = pn_df.shape[0]
    asbent_p_labels = [gene for gene in p_list if gene not in pn_df.index.tolist()]
    
    return([p_labels, n_labels, total_labels, asbent_p_labels])


def filter_tf(TF_file, tf_col, ranked_df):
    tf_df = pd.read_csv(TF_file, sep="\t")
    tf_ranked_df = tf_df.merge(ranked_df, left_on=tf_col, right_index=True, how="inner")
    tf_ranked_df.set_index(tf_col, inplace=True)

    return(tf_ranked_df)
    

def add_class(row, node_list):
    if row.name in node_list:
#     if row[1] in node_list:
        return 1.0
    else:
        return 0.0


def add_class_labels(pr_df, score_col, node_list, outfile, probs):
    
    if probs == True:
        ap_df = pr_df.sort_values(by=[score_col], ascending=False)
        ap_df[score_col + "_rerank"] = ap_df[score_col]
    else:
        ap_df = pr_df.sort_values(by=[score_col])
        ap_df[score_col + "_rerank"] = 1/ap_df[score_col]
        
    ap_df["class"] = ap_df.apply(add_class, args=([node_list]), axis=1)
    ap_df = ap_df.loc[:, ['class',score_col, score_col + "_rerank"]]
    ap_df.to_csv(outfile, sep="\t")
    
    return ap_df


def filter_thres(pre, rec, thres, class_df, score_col, score_df, thres_filter):
    
    t_min = []
    for p,r,t in zip(pre, rec, thres):
        if r <= thres_filter:
            t_min.append(t)

    if len(t_min) != 0:
        class_t_df = class_df[class_df[score_col + "_rerank"] >= min(t_min)]
        sub_score_df = score_df[score_df.index.isin(class_t_df.index)]
        sub_score_df = sub_score_df.sort_values(by=[score_col])
        return(sub_score_df)
    else:
        return pd.DataFrame()
        

def get_PR_metrics(sp, info, output_dir):
    # DLxJTK, JTK per, DL amp, DL per, DL both, DLxJTK TF, JTK per TF, DL amp TF, DL per TF, DL both TF
    # just need dlxjtk, and pydl (p_per_norm and dl_score) results files
    rankings = ["dlxjtk_score", "jtk_per_pval_norm", "dl_reg_pval_norm", "p_per_norm", "dl_score", "reg_score", "per_score"]
    
    print("Dataset: {}".format(sp))
    # p and n labels
    core_node_list = pd.read_csv(os.path.join(core_node_dir, info["core_file_col"][0]), index_col=0)
    core_node_list = core_node_list[info["core_file_col"][1]].tolist()

    dlxjtk_df = pd.read_csv(os.path.join(info["results"], dlxjtk_file), sep="\t", skiprows=3, index_col=0)
    dlxjtk_df.index.rename("genes", inplace=True)

    pydl_df = pd.read_csv(os.path.join(info["results"], pydl_file), sep="\t", skiprows=6, index_col=0)
    pydl_df.index.rename("genes", inplace=True)

    avg_percision_dic = {"full-Recall": [], "25%-Recall": [], "50%-Recall": []}
    rank_order = []

    p_labels, n_labels, total_labels, asbent_p_labels = P_and_N_labels(core_node_list, dlxjtk_df)
    print("\tPositive labels: {}\tNegative labels: {}\tTotal labels: {}\tAbsent core nodes in time series: {}".format(
        p_labels, n_labels, total_labels, ", ".join(asbent_p_labels)))

    tf_file = os.path.join(TF_dir, info["TF_file_col"][0])
    tf_labels_df = filter_tf(tf_file, info["TF_file_col"][1], dlxjtk_df)
    tf_labels_df.index.names = ['genes']

    tf_p_labels, tf_n_labels, tf_total_labels, tf_asbent_p_labels = P_and_N_labels(core_node_list, tf_labels_df)
    print("\tPositive labels: {}\tNegative labels: {}\tTotal labels: {}\tAbsent core nodes in TF list: {}".format(
        tf_p_labels, tf_n_labels, tf_total_labels, ", ".join(tf_asbent_p_labels)))
    
    ## PerReg score ###################################################
    PerReg_df = pydl_df.loc[:,["reg_score","per_score"]]
    rank = "dl_PerReg"
    PerReg_df[rank] = PerReg_df['reg_score'] * PerReg_df['per_score']
    
    class_outfile = os.path.join(output_dir, sp + "_" + rank + "_class.txt")
    
    class_df = add_class_labels(PerReg_df, rank, core_node_list, class_outfile, True)
    avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
    
    precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

    fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
    if fltered_25_df.empty:
        avg_per_25 = 0.0
    else:
        avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)

    fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
    if fltered_50_df.empty:
        avg_per_50 = 0.0
    else:
        avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

    avg_percision_dic["full-Recall"].append(avg_per_full)
    avg_percision_dic["25%-Recall"].append(avg_per_25)
    avg_percision_dic["50%-Recall"].append(avg_per_50)

    pr_df = pd.DataFrame()
    pr_df["Precision"] = precision
    pr_df["Recall"] = recall

    rank_order.append(rank)

    pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_P-R.txt"), sep="\t")
    
    rank_tf = rank + "_TF"
    print("\t" + rank_tf)
    # calc metrics
    # filter TFs
    tf_df = filter_tf(tf_file, info["TF_file_col"][1], PerReg_df)
    # calc metrics
    tf_df = tf_df.sort_values(by=[rank])
    tf_df.index.names = ['genes']

    class_outfile = os.path.join(output_dir, sp + "_" + rank_tf + "_class.txt")
    class_df = add_class_labels(tf_df, rank, core_node_list, class_outfile, True)

    avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
    precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

    fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
    if fltered_25_df.empty:
        avg_per_25 = 0.0
    else:
        avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)

    fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
    if fltered_50_df.empty:
        avg_per_50 = 0.0
    else:
        avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

    avg_percision_dic["full-Recall"].append(avg_per_full)
    avg_percision_dic["25%-Recall"].append(avg_per_25)
    avg_percision_dic["50%-Recall"].append(avg_per_50)

    tf_pr_df = pd.DataFrame()
    tf_pr_df["Precision"] = precision
    tf_pr_df["Recall"] = recall

    rank_order.append(rank_tf)

    tf_pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank_tf + "_P-R.txt"), sep="\t")    
    ################
    
    for rank in rankings:
        print("\t" + rank)
        if rank == "p_per_norm" or rank == "dl_score" or rank == "reg_score" or rank == "per_score":
            # calc metrics
            class_outfile = os.path.join(output_dir, sp + "_" + rank + "_class.txt")
            if rank == "reg_score" or rank == "per_score":
                class_df = add_class_labels(pydl_df, rank, core_node_list, class_outfile, True)
            else:
                class_df = add_class_labels(pydl_df, rank, core_node_list, class_outfile, False)
            
            avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
            precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

            fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
            if fltered_25_df.empty:
                avg_per_25 = 0.0
            else:
                avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)
                
            fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
            if fltered_50_df.empty:
                avg_per_50 = 0.0
            else:
                avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)
                
            avg_percision_dic["full-Recall"].append(avg_per_full)
            avg_percision_dic["25%-Recall"].append(avg_per_25)
            avg_percision_dic["50%-Recall"].append(avg_per_50)
        
            pr_df = pd.DataFrame()
            pr_df["Precision"] = precision
            pr_df["Recall"] = recall
            
            if rank == "p_per_norm":
                rank = "dl_p_per_norm"
            if rank == "per_score":
                rank = "dl_per_score"
            if rank == "reg_score":
                rank = "dl_reg_score"
                
            rank_order.append(rank)

            pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_P-R.txt"), sep="\t")

        else:
            # calc metrics
            class_outfile = os.path.join(output_dir, sp + "_" + rank + "_class.txt")
            class_df = add_class_labels(dlxjtk_df, rank, core_node_list, class_outfile, False)
            
            avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
            precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

            fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
            if fltered_25_df.empty:
                avg_per_25 = 0.0
            else:
                avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)
                
            fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
            if fltered_50_df.empty:
                avg_per_50 = 0.0
            else:
                avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

            avg_percision_dic["full-Recall"].append(avg_per_full)
            avg_percision_dic["25%-Recall"].append(avg_per_25)
            avg_percision_dic["50%-Recall"].append(avg_per_50)
        
            pr_df = pd.DataFrame()
            pr_df["Precision"] = precision
            pr_df["Recall"] = recall

            rank_order.append(rank)
            
            pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_P-R.txt"), sep="\t")
    
        class_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_class.txt"), sep="\t")

    for rank in rankings:
        rank_tf = rank + "_TF"
        print("\t" + rank_tf)
        # calc metrics
        if rank == "p_per_norm" or rank == "dl_score" or rank == "reg_score" or rank == "per_score":
            # filter TFs
            tf_df = filter_tf(tf_file, info["TF_file_col"][1], pydl_df)
            # calc metrics
            tf_df = tf_df.sort_values(by=[rank])
            tf_df.index.names = ['genes']

            class_outfile = os.path.join(output_dir, sp + "_" + rank_tf + "_class.txt")
            if rank == "reg_score" or rank == "per_score":
                class_df = add_class_labels(tf_df, rank, core_node_list, class_outfile, True)
            else:
                class_df = add_class_labels(tf_df, rank, core_node_list, class_outfile, False)
            
            avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
            precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

            fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
            if fltered_25_df.empty:
                avg_per_25 = 0.0
            else:
                avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)
                
            fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
            if fltered_50_df.empty:
                avg_per_50 = 0.0
            else:
                avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

            avg_percision_dic["full-Recall"].append(avg_per_full)
            avg_percision_dic["25%-Recall"].append(avg_per_25)
            avg_percision_dic["50%-Recall"].append(avg_per_50)
            
            tf_pr_df = pd.DataFrame()
            tf_pr_df["Precision"] = precision
            tf_pr_df["Recall"] = recall
            
            if rank_tf == "p_per_norm_TF":
                rank_tf = "dl_p_per_norm_TF"
            if rank_tf == "per_score_TF":
                rank_tf = "dl_per_score_TF"
            if rank_tf == "reg_score_TF":
                rank_tf = "dl_reg_score_TF"
                
            rank_order.append(rank_tf)
            
            tf_pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank_tf + "_P-R.txt"), sep="\t")

        else:
            # filter TFs
            tf_df = filter_tf(tf_file, info["TF_file_col"][1], dlxjtk_df)
            # calc metrics
            tf_df = tf_df.sort_values(by=[rank])
            tf_df.index.names = ['genes']

            class_outfile = os.path.join(output_dir, sp + "_" + rank_tf + "_class.txt")
            class_df = add_class_labels(tf_df, rank, core_node_list, class_outfile, False)
            
            avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
            precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

            fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
            if fltered_25_df.empty:
                avg_per_25 = 0.0
            else:
                avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)
                
            fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
            if fltered_50_df.empty:
                avg_per_50 = 0.0
            else:
                avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

            avg_percision_dic["full-Recall"].append(avg_per_full)
            avg_percision_dic["25%-Recall"].append(avg_per_25)
            avg_percision_dic["50%-Recall"].append(avg_per_50)
        
            tf_pr_df = pd.DataFrame()
            tf_pr_df["Precision"] = precision
            tf_pr_df["Recall"] = recall
            
            rank_order.append(rank_tf)
            
            tf_pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank_tf + "_P-R.txt"), sep="\t")
    
        class_df.to_csv(os.path.join(output_dir, sp + "_" + rank_tf + "_class.txt"), sep="\t")
        
    rank_order = [sp + "_" + rank for rank in rank_order]
    avg_percision_df = pd.DataFrame.from_dict(avg_percision_dic, orient='index', columns=rank_order)
    
    print(avg_percision_df)
    avg_percision_df.to_csv(os.path.join(output_dir, sp + "_avg_precision.txt"), sep="\t")
    

def get_PR_metrics_TFs(sp, info, output_dir):
    # DLxJTK, JTK per, DL amp, DL per, DL both, DLxJTK TF, JTK per TF, DL amp TF, DL per TF, DL both TF
    # just need dlxjtk, and pydl (p_per_norm and dl_score) results files
    rankings = ["dlxjtk_score", "jtk_per_pval_norm", "dl_reg_pval_norm", "p_per_norm", "dl_score", "reg_score", "per_score"]
    
    print("Dataset: {}".format(sp))
    
    dlxjtk_df = pd.read_csv(os.path.join(info["results"], dlxjtk_file), sep="\t", skiprows=3, index_col=0)
    dlxjtk_df.index.rename("genes", inplace=True)
    print(dlxjtk_df.shape)

    pydl_df = pd.read_csv(os.path.join(info["results"], pydl_file), sep="\t", skiprows=6, index_col=0)
    pydl_df.index.rename("genes", inplace=True)

    avg_percision_dic = {"full-Recall": [], "25%-Recall": [], "50%-Recall": []}
    rank_order = []

    tf_file = os.path.join(TF_dir, info["TF_file_col"][0])
    tf_labels_df = filter_tf(tf_file, info["TF_file_col"][1], dlxjtk_df)
    tf_genes_list =  tf_labels_df.index.tolist()
#     print(len(tf_genes_list))
    
    ## PerReg score ###################################################
    PerReg_df = pydl_df.loc[:,["reg_score","per_score"]]
    rank = "dl_PerReg"
    PerReg_df[rank] = PerReg_df['reg_score'] * PerReg_df['per_score']
    
    class_outfile = os.path.join(output_dir, sp + "_" + rank + "_class_idTF.txt")
    class_df = add_class_labels(PerReg_df, rank, tf_genes_list, class_outfile, True)
    
    avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
    precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

    fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
    if fltered_25_df.empty:
        avg_per_25 = 0.0
    else:
        avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)

    fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
    if fltered_50_df.empty:
        avg_per_50 = 0.0
    else:
        avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

    avg_percision_dic["full-Recall"].append(avg_per_full)
    avg_percision_dic["25%-Recall"].append(avg_per_25)
    avg_percision_dic["50%-Recall"].append(avg_per_50)

    pr_df = pd.DataFrame()
    pr_df["Precision"] = precision
    pr_df["Recall"] = recall

    rank_order.append(rank)

    pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_P-R_idTF.txt"), sep="\t")
    ################################################

    for rank in rankings:
        print("\t" + rank)
        if rank == "p_per_norm" or rank == "dl_score" or rank == "reg_score" or rank == "per_score":
            # calc metrics
            class_outfile = os.path.join(output_dir, sp + "_" + rank + "_class_idTF.txt")
            if rank == "reg_score" or rank == "per_score":
                class_df = add_class_labels(pydl_df, rank, tf_genes_list, class_outfile, True)
            else:
                class_df = add_class_labels(pydl_df, rank, tf_genes_list, class_outfile, False)
            
            avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
            precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

            fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
            if fltered_25_df.empty:
                avg_per_25 = 0.0
            else:
                avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)
                
            fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
            if fltered_50_df.empty:
                avg_per_50 = 0.0
            else:
                avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)
                
            avg_percision_dic["full-Recall"].append(avg_per_full)
            avg_percision_dic["25%-Recall"].append(avg_per_25)
            avg_percision_dic["50%-Recall"].append(avg_per_50)
        
            pr_df = pd.DataFrame()
            pr_df["Precision"] = precision
            pr_df["Recall"] = recall
            
            if rank == "p_per_norm":
                rank = "dl_p_per_norm"
            if rank == "per_score":
                rank = "dl_per_score"
            if rank == "reg_score":
                rank = "dl_reg_score"
                
            rank_order.append(rank)
            
            pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_P-R_idTF.txt"), sep="\t")

        else:
            # calc metrics
            class_outfile = os.path.join(output_dir, sp + "_" + rank + "_class_idTF.txt")
            class_df = add_class_labels(dlxjtk_df, rank, tf_genes_list, class_outfile, False)
            
            avg_per_full = average_precision_score(class_df["class"].values, class_df[rank + "_rerank"].values)
            precision, recall, threshold = precision_recall_curve(class_df["class"].values, class_df[rank + "_rerank"].values)

            fltered_25_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.25)
            if fltered_25_df.empty:
                avg_per_25 = 0.0
            else:
                avg_per_25 = average_precision_score(fltered_25_df["class"].values, fltered_25_df[rank + "_rerank"].values)
                
            fltered_50_df = filter_thres(precision, recall, threshold, class_df, rank, class_df, 0.50)
            if fltered_50_df.empty:
                avg_per_50 = 0.0
            else:
                avg_per_50 = average_precision_score(fltered_50_df["class"].values, fltered_50_df[rank + "_rerank"].values)

            avg_percision_dic["full-Recall"].append(avg_per_full)
            avg_percision_dic["25%-Recall"].append(avg_per_25)
            avg_percision_dic["50%-Recall"].append(avg_per_50)
        
            pr_df = pd.DataFrame()
            pr_df["Precision"] = precision
            pr_df["Recall"] = recall

            rank_order.append(rank)
            
            pr_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_P-R_idTF.txt"), sep="\t")
    
        class_df.to_csv(os.path.join(output_dir, sp + "_" + rank + "_class_idTF.txt"), sep="\t")

    rank_order = [sp + "_" + rank for rank in rank_order]
    avg_percision_df = pd.DataFrame.from_dict(avg_percision_dic, orient='index', columns=rank_order)
    
    print(avg_percision_df)
    avg_percision_df.to_csv(os.path.join(output_dir, sp + "_avg_precision_idTF.txt"), sep="\t")
    
    
def plot_PR_curve(pr_df):
    recall = pr_df["Recall"].values
    precision = pr_df["Precision"].values
    sub_p = pr_df["Precision"].values[:-1]
    max_p = max(sub_p)
    where : {'pre', 'post', 'mid'}
    step_kwargs = ({'step': 'post'} 
                if 'step' in signature(plt.fill_between).parameters 
                else {})
    plt.step(recall, precision, color='b', alpha=0.2, where='post')
    plt.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)
    plt.ylim((0.0, max_p*1.05))
    plt.xlim((0.0, 1.0))
#     plt.plot(recall, precision, linestyle="-", color="b", alpha=0.2)
#     plt.fill_between(recall, 0, precision, color='b', alpha=0.2)