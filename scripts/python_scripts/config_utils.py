import os
from configobj import ConfigObj


def str_to_bool(string):
    if string.lower() in ('true', 'yes', 't', '1'):
        return True
    elif string.lower() in ('false', 'no', 'n', '0'):
        return False
    else:
        raise ValueError


def parse_periods(config_periods):
    # in the case that only multiple periods were specified
    if isinstance(config_periods, list):
        return '"' + ', '.join(config_periods) + '"'

    # in the case that only one period was specified
    if isinstance(config_periods, str):
        return config_periods


def make_config(data_name, config_dir, ts_file, periods, results_dir, num_reg, num_per):
    config_dict = dict()
    config_dict['data_file'] = ts_file
    config_dict['output_dir'] = results_dir
    config_dict['num_proc'] = 5
    config_dict['verbose'] = True

    config_dict['dlxjtk_arguments'] = dict()
    config_dict['dlxjtk_arguments']['periods'] = periods
    config_dict['dlxjtk_arguments']['num_reg'] = num_reg
    config_dict['dlxjtk_arguments']['num_per'] = num_per

    co_obj = ConfigObj(config_dict)
    co_filename = os.path.join(config_dir, data_name + '_config.txt')
    print(co_filename)
    co_obj.filename = co_filename
    co_obj.write()


def default_arguments():
    def_arg_dict = dict()

    # default settings for DLxJTK
    def_arg_dict['dlxjtk_config'] = dict()
    def_arg_dict['dlxjtk_config']['num_reg'] = 1000000
    def_arg_dict['dlxjtk_config']['num_per'] = 0
    def_arg_dict['dlxjtk_config']['log_trans'] = True

    return def_arg_dict


def gen_dlxjtk_config(co):
    def_arg_dict = default_arguments()
    dlxjtk_config = ConfigObj(co['dlxjtk_arguments'])

    # Pass in pipeline config arguments needed by DLxJTK
    dlxjtk_config['data_file'] = co['data_file']
    dlxjtk_config['output_dir'] = os.path.join(co['output_dir'], 'node_finding_' + co['dlxjtk_datetime'])

    # Update dlxjtk configuration values
    dlxjtk_config['periods'] = parse_periods(dlxjtk_config['periods'])
    dlxjtk_config['dl_output_file'] = os.path.join(dlxjtk_config['output_dir'], 'pydl_results.tsv')
    dlxjtk_config['jtk_output_file'] = os.path.join(dlxjtk_config['output_dir'], 'pyjtk_results.tsv')
    dlxjtk_config['dlxjtk_output_file'] = os.path.join(dlxjtk_config['output_dir'], 'dlxjtk_results.tsv')
    dlxjtk_config['verbose'] = co['verbose']
    dlxjtk_config['num_proc'] = co['num_proc']

    # Populate DL and JTK config file with defaults if necessary
    for arg in ['num_reg', 'num_per', 'log_trans']:
        if arg not in dlxjtk_config:
            dlxjtk_config[arg] = def_arg_dict['dlxjtk_config'][arg]

    dlxjtk_config.filename = os.path.join(dlxjtk_config['output_dir'], 'dlxjtk_%s_config.txt' % co['dlxjtk_datetime'])

    return dlxjtk_config
