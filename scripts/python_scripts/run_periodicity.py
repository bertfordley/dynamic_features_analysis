import os
import ast
import datetime
import config_utils as utls
from configobj import ConfigObj


if __name__ == '__main__':
    """
    initialize a pipeline run with configuration file, setting defaults as necessary, and overwriting configuration 
    file values with optional command line arguments 
    """

    import argparse

    parser = argparse.ArgumentParser(prog='Data-to-Networks Pipeline',
                                     description='')

    # - required command-line argument -
    parser.add_argument('config_file',
                        action='store',
                        help='full path to configuration file')
    parser.add_argument('--verbose',
                        action='store',
                        help='verbose terminal output flag (default: True)')
    parser.add_argument('--num_proc',
                        action='store',
                        type=int,
                        help='integer specifying the number of MPI processes to use')
    args = parser.parse_args()

    # Load and parse master config file
    config_file = args.config_file
    config_file_name = os.path.basename(config_file)

    # parse config file
    def parse_co(section,key):
        try:
            results = ast.literal_eval(section[key])
        except:
            results = section[key]
        return results

    co = ConfigObj(ConfigObj(config_file).walk(parse_co))
    co['config_file'] = config_file

    # Overwrite executed/saved config file based on optional command-line inputs
    if args.verbose is not None:
        co['verbose'] = utls.str_to_bool(args.verbose)
    else:
        if co.get('verbose') is None:
            co['verbose'] = True
    if args.num_proc:
        co['num_proc'] = args.num_proc

    # Generate DLxJTK config file arguments
    co['dlxjtk_datetime'] = datetime.datetime.now().strftime('%y%m%d%H%M%S')
    dlxjtk_config = utls.gen_dlxjtk_config(co)

    # Compute DL regulator p-value
    if co['verbose']:
        print('\n  Computing DL regulator p-values')
        print('  -----------------------------')
    os.system('mpiexec -n %s python pydl/pydl.py %s -T %s -o %s -r %s -p %s -l %s -v %s' % (dlxjtk_config['num_proc'],
                                                                                            dlxjtk_config['data_file'],
                                                                                            dlxjtk_config['periods'],
                                                                                            dlxjtk_config['dl_output_file'],
                                                                                            dlxjtk_config['num_reg'],
                                                                                            dlxjtk_config['num_per'],
                                                                                            dlxjtk_config['log_trans'],
                                                                                            dlxjtk_config['verbose']))

    # Compute JTK periodicity p-value
    if co['verbose']:
        print('\n  Computing JTK periodicity p-values')
        print('  -------------------------------')
    os.system('python pyjtk/pyjtk.py %s -T %s -o %s' % (dlxjtk_config['data_file'],
                                                        dlxjtk_config['periods'],
                                                        dlxjtk_config['jtk_output_file']))

    # Combine DL and JTK p-values into the DLxJTK score
    if co['verbose']:
        print('\n  Computing DLxJTK scores')
        print('  -------------------------------')
    os.system('python python_scripts/dlxjtk.py %s %s %s' % (dlxjtk_config['dl_output_file'],
                                                       dlxjtk_config['jtk_output_file'],
                                                       dlxjtk_config['dlxjtk_output_file']))

    # Save a copy of the DLxJTK config file to track provenance
    dlxjtk_config.write()
