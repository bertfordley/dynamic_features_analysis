import argparse
import datetime
import numpy as np
import pandas as pd


def dlxjtk_func(row):
    # computes DLxJTK score for one gene in df
    amp = row["dl_reg_pval_norm"]
    per = row["jtk_per_pval_norm"]
    return per * amp * (1 + ((per / 0.001) ** 2)) * (1 + ((amp / 0.001) ** 2))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='DLxJTK function',
                                     description='computes DLxJTK score from DL and JTK p-values to rank transcriptional regulatory elements')
    parser.add_argument('pydl_file',
                        action='store',
                        help='output results of pyDL run')
    parser.add_argument('pyjtk_file',
                        action='store',
                        help='output results of pyJTK run')
    parser.add_argument('dlxjtk_file',
                        action='store',
                        help='output results of combined DLxJTK')

    args = parser.parse_args()

    # parse file names and locations to create output files
    pydl_file = args.pydl_file
    pyjtk_file = args.pyjtk_file
    dlxjtk_file = args.dlxjtk_file

    # load dl and jtk results into dataframes
    dl_df = pd.read_csv(pydl_file, delimiter='\t', index_col=0, comment='#')
    dl_df.rename(columns={'p_reg': 'dl_reg_pval', 'p_reg_norm': 'dl_reg_pval_norm'}, inplace=True)

    jtk_df = pd.read_csv(pyjtk_file, delimiter='\t', index_col=0, comment='#')
    jtk_df.rename(columns={'p-value': 'jtk_per_pval'}, inplace=True)

    # normalize jtk p-values for use in dlxjtk score
    jtk_df['jtk_per_pval_norm'] = jtk_df['jtk_per_pval'] / np.median(jtk_df['jtk_per_pval'])

    # merge dl and jtk dataframes
    dlxjtk_df = pd.merge(dl_df, jtk_df, left_index=True, right_index=True)
    dlxjtk_df = dlxjtk_df[['dl_reg_pval', 'dl_reg_pval_norm', 'jtk_per_pval', 'jtk_per_pval_norm']]

    # compute dlxjtk score and sort genes by the score
    dlxjtk_df['dlxjtk_score'] = dlxjtk_df.apply(dlxjtk_func, axis=1)
    dlxjtk_df.sort_values(by='dlxjtk_score', axis=0, ascending=True, inplace=True)

    # write file
    with open(dlxjtk_file, 'w') as f:
        f.write('# DLxJTK regulatory element scores computed on %s\n' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        f.write("# pyDL file: %s\n" % pydl_file)
        f.write("# pyJTK file: %s\n" % pyjtk_file)
        dlxjtk_df.to_csv(f, sep='\t', header=True, index=True)

