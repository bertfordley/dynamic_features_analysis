data_file = /Users/robertmoseley/Desktop/Pipeline/DLxJTK_paper/dynamic_features_analysis/time_series_data/cell_cycle/Scerevisiae_alphafactor_245-5min_RNAseq_dropped.tsv
output_dir = /Users/robertmoseley/Desktop/Pipeline/DLxJTK_paper/dynamic_features_analysis/Reproduce_Results/cell_cycle/Scerevisiae/rnaseq
num_proc = 5
verbose = True
[dlxjtk_arguments]
    periods = 70, 75, 80
    num_reg = 100000
    num_per = 10000
