data_file = /Users/robertmoseley/Desktop/Pipeline/DLxJTK_paper/dynamic_features_analysis/time_series_data/circadian_clock/athaliana_LDHC_48-4_microarray.tsv
output_dir = /Users/robertmoseley/Desktop/Pipeline/DLxJTK_paper/dynamic_features_analysis/Results/Arth_Permutations_test/athaliana/LDHC
num_proc = 5
verbose = True
[dlxjtk_arguments]
    periods = 20, 24, 28
    num_reg = 20000000
    num_per = 0
